export interface State {
  layers: [Layer]
}

export interface Layer {
  title: string,
  size: string,
  id: string,
  active: boolean
}

export interface Action {
  type: string,
  payload?: any
}
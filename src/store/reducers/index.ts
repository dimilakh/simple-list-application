import { combineReducers } from 'redux'
​
import layers from './layersList'

export default combineReducers({
  layers
})

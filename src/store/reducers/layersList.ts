import * as uuid from 'uuid/v1'

import { Layer, Action } from './index.d'
import * as actionTypes from '../../constants/actionTypes'

const initialState: Layer[] = []

const layers = (state: Layer[] = initialState , action: Action) => {
  switch (action.type) {
    case actionTypes.RECEIVE_LAYERS_LIST:
      return [ ...action.payload ]

    case actionTypes.UPDATE_LAYER_TITLE:
      return state.map((layer: Layer) => {
        if (layer.id === action.payload.id) {
          return { ...layer, title: action.payload.title }
        }
        return layer
      })

    case actionTypes.TOGGLE_LAYER_BY_ID:
      return state.map((layer: Layer) => {
        if (layer.id === action.payload) {
          return { ...layer, active: !layer.active }
        }
        return layer
      })

    case actionTypes.REMOVE_LAYER_BY_ID:
      return state.filter((layer: Layer) => layer.id !== action.payload)

    case actionTypes.CREATE_NEW_LAYER:
      return [
        ...state,
        {
          active: false,
          id: uuid(),
          title: 'New layer'
        }
      ]
    default:
      return state
  }
}
​
export default layers

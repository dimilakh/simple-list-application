import { Action, Layer } from './reducers/index.d'
import * as actionTypes from '../constants/actionTypes'

export const receiveList = (itemsList: [Layer]): Action => ({
  payload: itemsList,
  type: actionTypes.RECEIVE_LAYERS_LIST
})

export const requestList = (): Action => ({
  type: actionTypes.API_REQUEST_LIST
})

export const toggleLayer = (id: string): Action => ({
  payload: id,
  type: actionTypes.TOGGLE_LAYER_BY_ID
})

export const createLayer = (): Action => ({
  type: actionTypes.CREATE_NEW_LAYER
})

export const removeLayer = (id: string): Action => ({
  payload: id,
  type: actionTypes.REMOVE_LAYER_BY_ID
})

export const updateLayerTitle = (id: string, title: string): Action => ({
  payload: {
    id,
    title
  },
  type: actionTypes.UPDATE_LAYER_TITLE
})

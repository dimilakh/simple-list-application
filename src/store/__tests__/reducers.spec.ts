import reducer from '../reducers/layersList'
import { Layer } from '../reducers/index.d'
import * as actions from '../actions'

const mockedState = [] as Layer[]
const customMockedState = [
  {
    active: false,
    id: '1',
    title: 'New layer'
  } as Layer
]

describe('Layer reducer', () => {
  it('should return initial state', () => {
    const nextState = reducer(undefined, {} as any)

    expect(nextState).toEqual(mockedState)
  })

  it('should add new layer to the state', () => {
    const nextState = reducer(mockedState, actions.createLayer())
    const expectedState = [
      {
        active: false,
        id: expect.any(String),
        title: 'New layer'
      }
    ]

    expect(nextState).toEqual(expectedState)
  })

  it('should make layer active', () => {
    const nextState = reducer(customMockedState, actions.toggleLayer('1'))
    const expectedState = [{
      active: true,
      id: '1',
      title: 'New layer'
    }]
    expect(nextState).toEqual(expectedState)
  })

  it('should remove layer by id', () => {
    const nextState = reducer(customMockedState, actions.removeLayer('1'))

    expect(nextState).toHaveLength(0)
  })
})

import { call, put, takeLatest } from 'redux-saga/effects'
import { API_REQUEST_LIST } from '../constants/actionTypes'
import { receiveList } from './actions'
import { fetchList } from '../services'

function* getLayersList() {
  try {
    const list = yield call(fetchList)

    yield put(receiveList(list))
  } catch (err) {
    throw err
  }
}

export default function* mainSaga() {
  yield takeLatest(API_REQUEST_LIST, getLayersList)
}

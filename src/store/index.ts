import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import logger from 'redux-logger'

import rootReducer from './reducers'
import mainSaga from './sagas'

const sagaMiddleware = createSagaMiddleware()
const middlewareList = [ sagaMiddleware ]

if (process.env.NODE_ENV === `development`) {
  middlewareList.push(logger as any)
}

const store = createStore(
  rootReducer,
  applyMiddleware(...middlewareList)
)

sagaMiddleware.run(mainSaga)

export default store

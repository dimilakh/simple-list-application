export const fetchList = async () => {
  try {
    const res = await fetch(process.env.API_BASE + '/layers')

    return res.json()
  } catch (err) {
    throw err
  }
}

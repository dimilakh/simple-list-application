import { Layer } from './store/reducers/index.d'

export interface Props {
  layerList: [Layer],
  fetchLayerList(): void,
  updateLayerTitle(id: string, title: string): void,
  removeLayer(id: string): void,
  toggleLayer(id: string): void,
  createLayer(): void
}
export interface Props {
  size?: string
  title: string,
  id: string,
  active: boolean,
  updateLayerTitle(id: string, title: string): void,
  removeLayer(id: string): void,
  toggleLayer(id: string): void
}

export interface State {
  edit: boolean
}
import './ListItem.sass'

import * as React from 'react'
import Popup from 'reactjs-popup'

import { FaPencilAlt, FaTrashAlt, FaCheck } from 'react-icons/fa'
import { Props, State } from './index.d'

class ListItem extends React.Component<Props, State> {
  public state = {
    edit: false
  }

  private inputRef: React.RefObject<HTMLInputElement>

  constructor(props: Props) {
    super(props)

    this.inputRef = React.createRef()

    this.handleEditClick = this.handleEditClick.bind(this)
    this.handleDeleteClick = this.handleDeleteClick.bind(this)
    this.handleToggleClick = this.handleToggleClick.bind(this)
    this.handleInputChange = this.handleInputChange.bind(this)
  }

  public componentDidUpdate() {
    if (this.state.edit) {
      this.inputRef.current.focus()
    }
  }

  public handleInputChange(e: React.FormEvent<HTMLInputElement>) {
    const { id, updateLayerTitle } = this.props

    updateLayerTitle(id, e.currentTarget.value)
  }

  public handleEditClick(): void {
    this.setState({
      edit: !this.state.edit
    })
  }

  public handleDeleteClick(): void {
    const { id, removeLayer } = this.props

    removeLayer(id)
  }

  public handleToggleClick(): void {
    const { id, toggleLayer } = this.props

    toggleLayer(id)
  }

  public render() {
    const { edit } = this.state
    const { title, active } = this.props

    const renderEditIcon = edit ?
      <FaCheck onClick={this.handleEditClick} className='edit-icon' size={16} /> :
      <FaPencilAlt onClick={this.handleEditClick} className='edit-icon' size={16} />

    const renderRemoveIcon = (
      <Popup
        modal={true}
        closeOnDocumentClick={true}
        trigger={
          <FaTrashAlt
            className='remove-icon'
            size={16}
          />
        }
      >
        { (close: any) =>
          <div className='modal'>
            <h3>Do you want to remove this layer?</h3>
            <div className='modal-controls'>
              <button onClick={this.handleDeleteClick}>YES</button>
              <button onClick={close}>NO</button>
            </div>
          </div>
        }
      </Popup>
    )

    const isActiveClass = active ? 'active' : ''

    return (
      <li className='list-item'>
        <div className='item-controls'>
          <div className={`indicator ${isActiveClass}`} onClick={this.handleToggleClick} />
          <input
            value={title}
            disabled={!edit}
            ref={this.inputRef}
            onChange={this.handleInputChange}
            className='title-input'
          />
        </div>
        <div className='item-controls'>
          {renderEditIcon}
          {renderRemoveIcon}
        </div>
      </li>
    )
  }
}

export default ListItem

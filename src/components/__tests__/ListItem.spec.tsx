import * as React from 'react'
import { shallow } from 'enzyme'

import ListItem from '../ListItem/index'

const mockedFunction = () => jest.fn()

describe('ListItem', () => {
  it('should render render component with title', () => {
    const componentTitle = 'Layer'
    const component = shallow(
      <ListItem
        title={componentTitle}
        id='1'
        active={false}
        updateLayerTitle={mockedFunction}
        removeLayer={mockedFunction}
        toggleLayer={mockedFunction}
      />
    )

    expect(component.find('.title-input').props().value).toEqual(componentTitle)
  })
})

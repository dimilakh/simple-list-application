import * as React from 'react'
import ListItem from './components/ListItem'
import { connect } from 'react-redux'
import { State, Layer } from './store/reducers/index.d'
import { requestList, updateLayerTitle, toggleLayer, removeLayer, createLayer } from './store/actions'
import { Props } from './index.d'

class App extends React.Component<Props> {
  constructor(props: Props) {
    super(props)

    this.handleNewItemClick = this.handleNewItemClick.bind(this)
  }

  public componentDidMount() {
    this.props.fetchLayerList()
  }

  public handleNewItemClick() {
    this.props.createLayer()
  }

  public render() {
    return (
      <div className='container'>
      <ul>
        {this.props.layerList.map((layer: Layer) => (
          <ListItem
            key={layer.id}
            updateLayerTitle={this.props.updateLayerTitle}
            removeLayer={this.props.removeLayer}
            toggleLayer={this.props.toggleLayer}
            {...layer}
          />
        ))}
      </ul>
      <button className='add-button' onClick={this.handleNewItemClick}>New item</button>
    </div>
    )
  }
}

const mapStateToProps = (state: State) => ({
  layerList: state.layers
})

const mapDispatchToProps = (dispatch: any) => ({
  createLayer: () => dispatch(createLayer()),
  fetchLayerList: () => dispatch(requestList()),
  removeLayer: (id: string) => dispatch(removeLayer(id)),
  toggleLayer: (id: string) => dispatch(toggleLayer(id)),
  updateLayerTitle: (id: string, title: string) => dispatch(updateLayerTitle(id, title))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)
